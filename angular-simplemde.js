'use strict';

angular.module('ngSimpleMDE', [])
    .directive('simplemde', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: ['ngModel'],
            scope: {
                options: '=simplemde'
            },
            link: function (scope, element, attrs, ctrls) {
                var ngModel = ctrls[0],
                    simplemde,
                    options = {
                        spellChecker: false,
                        tabSize: 4
                    };
                
                if (angular.isObject(scope.options)) {
                    angular.extend(options, scope.options);
                }

                options.element = element[0];

                simplemde = new SimpleMDE(options);

                simplemde.codemirror.on('change', function () {
                    ngModel.$setViewValue(simplemde.value());
                });

                ngModel.$render = function () {
                    simplemde.value(ngModel.$modelValue !== null ? ngModel.$modelValue : '');
                };
            }
        };
    }]);