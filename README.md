# angular-simplemde

Angular directive for [SimpleMDE Markdown Editor](https://github.com/NextStepWebs/simplemde-markdown-editor)

## Usage

See `examples/demo.html`